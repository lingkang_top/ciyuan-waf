<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>nginx日志</title>
  <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding: 20px">
<div class="layui-form layui-row layui-col-space16" style="margin: 15px 0">
  <div class="layui-col-md6">
    <div class="layui-form-item" style="margin-bottom: 0;">
      <label class="layui-form-label">日志路径</label>
      <div class="layui-input-block">
        <input type="text" id="path" value="${path}" placeholder="请输入日志目录路径" class="layui-input">
      </div>
    </div>
  </div>
  <div class="layui-col-md6">
    <button class="layui-btn" id="update">
      <i class="layui-icon layui-icon-edit"></i>
      更新日志路径
    </button>
    <button class="layui-btn" onclick="window.location.reload()">
      <i class="layui-icon layui-icon-refresh-3"></i>
      刷新列表
    </button>
  </div>
</div>
<div class="layui-card" style="margin-top: 10px;">
  <table class="layui-table">
    <colgroup>
      <col width="350">
      <col width="180">
      <col width="180">
      <col>
    </colgroup>
    <thead>
    <tr>
      <th>日志文件</th>
      <th>大小</th>
      <th>最后更新时间</th>
      <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list list as item>
      <tr>
        <td>${item.name}</td>
        <td class="size">${item.size}</td>
        <td>${item.lastUpdateTime?string("yyyy-MM-dd HH:mm:ss")}</td>
        <td>
          <button type="button" fname="${item.name}"
                  class="download layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon layui-icon-download-circle"></i> 下 载
          </button>
          <button fname="${item.name}" type="button"
                  class="see layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon layui-icon-eye"></i> 查 看
          </button>
        </td>
      </tr>
    </#list>
    </tbody>
  </table>
</div>

<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['jquery'], function () {
        var $ = layui.jquery
        layer.load(2, {icon: 16, shade: 0.1, time: 500});

        $('#update').click(function () {
            var this_ = this;
            layer.confirm('确认要更新此日志目录吗？', {
                btn: ['确定', '关闭'] //按钮
            }, function () {
                $.ajax({
                    data: {path: $('#path').val()},
                    type: 'post',
                    url: '/nginx/log/updatePath',
                    success(res) {
                        layer.msg(res.msg)
                        if (res.code === 0)
                            setTimeout(function () {
                                window.location.reload()
                            }, 2000)
                    }
                })
            });
        })
        $('.download').click(function () {
            var fname = $(this).attr('fname')
            window.open('/nginx/log/download?name=' + btoa(fname))
        })
        $('.see').click(function () {
            var fname = $(this).attr('fname')
            window.open('/nginx/log/see?name=' + btoa(fname))
        })
        var mb = 1024
        $('.size').each(function () {
            var ele = $(this);
            if (ele === '0')
                ele.html('0kb')
            else
                ele.html((ele.html() / mb).toFixed(4) + 'kb')
        })
    });

</script>
</body>
</html>
<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>操作日志</title>
  <link rel="stylesheet" href="/component/pear/css/pear.css">
  <style>
    .content {
      cursor: pointer;
    }
  </style>
</head>
<body style="padding: 20px">
<div class="layui-form">
  <div class="layui-row layui-col-space16">
    <div class="layui-col-md3">
      <div class="layui-input-group">
        <div class="layui-input-prefix">
          类型
        </div>
        <select name="type">
          <option value="">全部类型</option>
            <#list type as item>
              <option value="${item.code}">${item.name}</option>
            </#list>
        </select>
      </div>
    </div>
    <div class="layui-col-md2">
      <div class="layui-input-group">
        <div class="layui-input-prefix layui-input-split">
          IP
        </div>
        <input type="text" name="ip" placeholder="ip" class="layui-input">
      </div>
    </div>
    <div class="layui-col-md7">
      <div class="layui-input-group">
        <div class="layui-input-prefix layui-input-split">
          时间范围
        </div>
        <div class="layui-inline" id="ID-laydate-range">
          <div class="layui-input-inline">
            <input type="text" name="start" autocomplete="off" id="ID-laydate-start-date" class="layui-input"
                   placeholder="开始日期">
          </div>
          <div class="layui-input-inline">
            <input type="text" name="end" autocomplete="off" id="ID-laydate-end-date" class="layui-input"
                   placeholder="结束日期">
          </div>
        </div>
      </div>
    </div>


    <div class="layui-col-md12">
      <div class="layui-input-block">
        <button type="submit" class="layui-btn" lay-submit lay-filter="search">搜索</button>
      </div>
    </div>
  </div>
</div>

<table class="layui-hide" id="test" lay-filter="test"></table>

<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    function fDate(date) {
        let year = date.getFullYear(); // 获取年份
        let month = date.getMonth() + 1; // 获取月份（注意月份从0开始，所以我们需要加1）
        let day = date.getDate(); // 获取日期
        let hours = date.getHours(); // 获取小时
        let minutes = date.getMinutes(); // 获取分钟
        let seconds = date.getSeconds(); // 获取秒数

        // 如果月份、日期、小时、分钟或秒数小于10，前面添加一个"0"以确保它们总是两位数
        month = (month < 10 ? "0" : "") + month;
        day = (day < 10 ? "0" : "") + day;
        hours = (hours < 10 ? "0" : "") + hours;
        minutes = (minutes < 10 ? "0" : "") + minutes;
        seconds = (seconds < 10 ? "0" : "") + seconds;

        return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
    }

    var types = {
        <#list type as item>
        ${item.code}: '${item.name}',
        </#list>
    }
    layui.use(['jquery'], function () {
        var $ = layui.jquery
        var table = layui.table;
        var form = layui.form;
        layer.load(2, {icon: 16, shade: 0.1, time: 500});
        // 创建渲染实例
        table.render({
            elem: '#test',
            url: '/nginx/history/list', // 此处为静态模拟数据，实际使用时需换成真实接口
            page: true,
            limits:[10,20,50],
            height: '540px',
            cols: [[
                {type: 'checkbox', fixed: 'left'},
                {
                    field: 'id', width: 140, title: 'ID', sort: true
                },
                {
                    field: 'type', width: 120, title: '类型', sort: true, templet: function (d) {
                        return types[d.type];
                    }
                },
                // 模板 - 函数写法
                {
                    field: 'ip', width: 120, title: 'IP', sort: true
                },
                {
                    field: 'createTime', width: 170, title: '时间', sort: true, templet: function (d) {
                        return fDate(new Date(d.createTime));
                    }
                },
                {
                    field: 'content', width: 300, title: '内容', sort: true, class: 'acc'
                }
            ]],
            parseData: function (res) {
                return {
                    "code": res.code, // 解析接口状态
                    "msg": res.msg, // 解析提示文本
                    "data": res.data, // 解析数据列表
                    "count": res.total,
                };
            }
        });

        $('tbody').click(function (e) {
            if ($(e.target).parent().attr('data-field') === 'content') {
                layer.open({
                    type: 1,
                    area: ['50%', '240px'], // 宽高
                    shadeClose: true,
                    content: '<div style="padding: 20px">' + $(e.target).html() + '</div>'
                });
            }
        })

        form.on('submit(search)', function (data) {
            var field = data.field; // 获取表单字段值
            layer.load(2, {icon: 16, shade: 0.1, time: 500});
            table.reloadData('test', {
                where: {
                    ip: field.ip,
                    type: field.type,
                    start: field.start,
                    end: field.end
                }
            }, true);
            return false; // 阻止默认 form 跳转
        });

        var laydate = layui.laydate;
        // 日期范围 - 左右面板独立选择模式
        laydate.render({
            elem: '#ID-laydate-range',
            range: ['#ID-laydate-start-date', '#ID-laydate-end-date'],
            type: 'datetime'
        });

    });

</script>
</body>
</html>
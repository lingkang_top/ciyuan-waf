<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>添加Server配置</title>
    <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding: 10px">
<form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label">文件名</label>
        <div class="layui-input-inline layui-input-wrap" style="width: 400px">
            <input type="text" name="name" lay-verify="required" autocomplete="off" value="${name!}"
                   placeholder="配置名称，将生成位于confList目录下" class="layui-input">
        </div>
        <#if !edit??>
            <div class="layui-form-mid layui-text-em">自动追加 <b style="color: red">.conf</b> 后缀</div>
        </#if>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">配置内容</label>
        <div class="layui-input-block">
      <textarea placeholder="生成的xxx.conf将被写入nginx.conf中的http内" name="content" id="edit" rows="17"
                style="width: 90%;font-size: 16px;line-height: 22px;padding: 10px;background-color: antiquewhite;font-weight:bold"
                aria-hidden="true">${content!}</textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" lay-submit lay-filter="demo1">保存提交</button>
            <button id="template" type="button" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon layui-icon-template-1"></i> 配置模版例示
            </button>
        </div>
    </div>
</form>

<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['form', 'jquery'], function () {
        var $ = layui.jquery
        var form = layui.form
        layer.load(2, {icon: 16, shade: 0.1, time: 500});
        // 提交事件
        form.on('submit(demo1)', function (data) {
            var field = data.field; // 获取表单字段值
            // 显示填写结果，仅作演示用
            // layer.alert(JSON.stringify(field), {
            //     title: '当前填写的字段值'
            // });
            <#if edit??>
            $.ajax({
                data: field,
                type: 'post',
                url: '/nginx/server/editConfig',
                success(res) {
                    if (res.code === 0) {
                        layer.msg(res.msg)
                        setTimeout(function () {
                            parent.location.reload()
                        }, 2500)
                    } else {
                        console.log(res.msg)
                        layer.alert(res.msg, {
                            title: '错误',
                            area: ['420px', '240px']
                        })

                    }
                }
            })
            <#else>
            var loadIndex = layer.msg('添加中...', {
                icon: 16,
                shade: 0.5
            });
            $.ajax({
                data: field,
                type: 'post',
                url: '/nginx/server/addConfig',
                success(res) {
                    if (res.code === 0) {
                        layer.msg(res.msg)
                        setTimeout(function () {
                            parent.location.reload()
                        }, 2500)
                    } else {
                        console.log(res.msg)
                        layer.alert(res.msg, {
                            title: '错误',
                            area: ['420px', '240px']
                        })
                    }
                }
            })
            </#if>

            return false; // 阻止默认 form 跳转
        });

        $('#template').click(function () {
            window.open('/view/nginx/template.html')
        })
    });
</script>
</body>
</html>
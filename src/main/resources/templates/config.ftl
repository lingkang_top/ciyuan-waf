<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nginx配置</title>
    <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding: 10px;">
<div class="layui-card">
    <div style="display: none;margin-top: 15px;margin-left: 34px;margin-bottom: 20px" id="action">
        <button id="refresh" type="button"
                class="layui-btn layui-btn-sm layui-bg-blue">
            <i class="layui-icon layui-icon-refresh-3"></i> 刷新状态
        </button>
        <b style="margin-left: 10px">
            nginx运行状态：<span id="statusRun" style="color: green;display: none">运行</span>
            <span id="statusStop" style="color: red;display: none">停止</span>
        </b>
        <br/>
        <br/>
        <button class="layui-btn" id="restart">
            <i class="layui-icon layui-icon-refresh"></i>
            启动/重启 nginx
            <span id="needReset" style="display: none" class="layui-badge">存在修改未重启</span>
        </button>
        <button class="layui-btn layui-bg-red" id="stop" style="display: none;margin-left: 20px">
            <i class="layui-icon layui-icon-error"></i>
            停止 nginx
        </button>
    </div>
    <div class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">配置说明</label>
            <div class="layui-input-block" style="line-height: 36px">
                <a href="/view/nginx/config.html" target="_blank" style="color: blue">配置路径说明</a>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">nginx二进制目录</label>
            <div class="layui-input-block">
                <input type="text" id="path" value="${data.nginxPath!}"
                       placeholder="window下是nginx.exe所在的目录，Linux是nginx二进制启动文件，例如d:\\nginx-1.21.4.2-win64、/usr/sbin"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">nginx.conf目录</label>
            <div class="layui-input-block">
                <input type="text" id="conf" value="${data.confPath!}"
                       placeholder="nginx.conf所在的目录，为空时默认nginx目录下的conf目录，例如d:\\nginx-1.21.4.2-win64\\conf、/etc/nginx"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"></label>
            <div class="layui-input-block">
                <div class="flex-column" style="line-height: 22px;width: 150px">
                    <#if data.isInit!="true">
                        <span class="layui-badge" style="width: 50px;margin-bottom: 10px">未初始化</span>
                        <button id="selectPath" style="margin-bottom: 10px" plain class="pear-btn pear-btn-primary">
                            目录浏览
                        </button>
                        <button id="initBtn" plain class="pear-btn pear-btn-primary">
                            <i class="layui-icon layui-icon-link"></i>
                            执行Nginx绑定
                        </button>
                    <#else>
                        <span class="layui-badge layui-bg-blue" style="width: 50px;margin-bottom: 10px">已初始化</span>
                        <button id="unBind" plain class="pear-btn pear-btn-danger">
                            <i class="layui-icon layui-icon-unlink"></i>
                            解除Nginx绑定
                        </button>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['table', 'form', 'jquery']);
    var table = layui.table;
    var form = layui.form;
    var $ = layui.jquery
    layer.load(2, {icon: 16, shade: 0.1, time: 800});
    <#if data.isInit=="true">
    $('#path').attr('disabled', true);
    $('#conf').attr('disabled', true);
    $('#action').show()
    $('#restart').click(function () {
        var loadIndex = layer.msg('启动/重启中...', {
            icon: 16,
            shade: 0.5
        });
        $.ajax({
            url: '/nginx/restart',
            type: 'post',
            success(res) {
                layer.close(loadIndex)
                if (res.code === 0) {
                    layer.msg(res.msg)
                    setTimeout(function () {
                        window.location.reload()
                    }, 2000)
                } else {
                    layer.alert(res.msg, {
                        title: '错误',
                        area: ['420px', '240px']
                    })
                }
            }
        })
    })
    </#if>
    <#if nginxExistsUpdate==true>
    $('#needReset').show()
    <#else>
    $('#needReset').hide()
    </#if>

    function status() {
        $.ajax({
            url: '/nginx/nginxStatus',
            success(res) {
                if (res.code === 0) {
                    if (res.data) {
                        $('#statusRun').show()
                        $('#stop').show()
                    } else
                        $('#statusStop').show()
                }
            }
        })
    }

    status()
    $('#refresh').click(function () {
        layer.load(2, {icon: 16, shade: 0.1, time: 1000});
        status()
    })


    $('#stop').click(function () {
        var loadIndex = layer.msg('停止nginx中...', {
            icon: 16,
            shade: 0.5
        });
        $.ajax({
            url: '/nginx/stop',
            type: 'post',
            success(res) {
                if (res.code === 0) {
                    layer.msg(res.msg)
                    setTimeout(function () {
                        window.location.reload()
                    }, 1500)
                } else {
                    layer.alert(res.msg, {
                        title: '错误',
                        area: ['420px', '240px']
                    })
                }
            }
        })
    })

    $('#initBtn').click(function () {
        if ($('#path').val() === '') {
            layer.msg('Nginx所在目录路径不能为空！')
            return
        }
        layer.confirm('确认要绑定此路径nginx？绑定后conf/nginx.conf将被替换', {
            btn: ['确定', '关闭'] //按钮
        }, function () {
            var loadIndex = layer.msg('绑定nginx中...', {
                icon: 16,
                shade: 0.5
            });
            $.ajax({
                url: '/nginx/config',
                data: {path: $('#path').val(), conf: $('#conf').val()},
                type: 'post',
                success(res) {
                    layer.msg(res.msg)
                    if (res.code === 0) {
                        setTimeout(function () {
                            window.location.reload()
                        }, 1500)
                    }
                }
            })
        });
    })

    $('#unBind').click(function () {
        if ($('#path').val() === '') {
            layer.msg('还未绑定Nginx！')
            return
        }
        layer.confirm('确认要解绑此路径nginx？绑定后将无法管理nginx，请注意备份', {
            btn: ['确定', '关闭'] //按钮
        }, function () {
            var loadIndex = layer.msg('解绑nginx中...', {
                icon: 16,
                shade: 0.5
            });
            $.ajax({
                url: '/nginx/unconfig',
                type: 'post',
                success(res) {
                    layer.msg(res.msg)
                    if (res.code === 0) {
                        setTimeout(function () {
                            window.location.reload()
                        }, 1500)
                    }
                }
            })
        });
    })

    $('#selectPath').click(function () {
        // var path = $('#path').val()
        var menu=parent.$('#sideMenu').find('a[menu-id="1"]')
        menu.click()
        /*layer.open({
            type: 2,
            title: '目录浏览',
            shadeClose: true,
            shade: 0.6,
            area: ['80%', '80%'],
            content: '/file/pathSelect?isFolder=true&path=' + btoa(path) // iframe 的 url
        });*/
    })
</script>
</body>
</html>
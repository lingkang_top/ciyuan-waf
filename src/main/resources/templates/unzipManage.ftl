<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>解压文件管理</title>
  <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding: 20px">
<div class="layui-card" style="margin-top: 10px;">
  <table class="layui-table">
    <colgroup>
      <col width="150">
      <col width="350">
      <col width="180">
      <col width="120">
      <col width="180">
      <col>
    </colgroup>
    <thead>
    <tr>
      <th>id</th>
      <th>存放路径</th>
      <th>原名</th>
      <th>大小</th>
      <th>上传时间</th>
      <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list list as item>
      <tr>
        <td>${item.id}</td>
        <td>${item.path}</td>
        <td>${item.originalName}</td>
        <td class="size">${item.size}</td>
        <td>${item.createTime?string("yyyy-MM-dd HH:mm:ss")}</td>
        <td>
          <button type="button" fid="${item.id}"
                  class="download layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon layui-icon-download-circle"></i> 下 载
          </button>
          <button fid="${item.id}" type="button"
                  class="delete layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon layui-icon-delete"></i> 删 除
          </button>
            <#if item.suffix=='zip'>
              <button fid="${item.id}" type="button"
                      class="unzip layui-btn layui-btn-sm">
                <i class="layui-icon layui-icon-template-1"></i> 解 压
              </button>
            </#if>
        </td>
      </tr>
    </#list>
    </tbody>
  </table>
</div>

<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['jquery'], function () {
        var $ = layui.jquery
        layer.load(2, {icon: 16, shade: 0.1, time: 500});
        $('.delete').click(function () {
            var this_ = this;
            layer.confirm('确认要删除此文件吗？', {
                btn: ['确定', '关闭'] //按钮
            }, function () {
                var fid = $(this_).attr('fid')
                $.ajax({
                    data: {id: fid},
                    type: 'post',
                    url: '/file/delete',
                    success(res) {
                        layer.msg(res.msg)
                        if (res.code === 0)
                            setTimeout(function () {
                                window.location.reload()
                            }, 2500)
                    }
                })
            });
        })
        $('.download').click(function () {
            var fid = $(this).attr('fid')
            window.open('/file/download?id=' + fid)
        })
        $('.unzip').click(function () {
            var fid = $(this).attr('fid')
            layer.prompt({
                title: '请输入解压到的路径（覆盖）',
                placeholder: "路径不存在时自动创建",
                formType: 0
            }, function (val, index) {
                layer.load(2, {icon: 16, shade: 0.1, time: 3000});
                $.ajax({
                    data: {id: fid, path: val},
                    type: 'post',
                    url: '/file/unzip',
                    async:true,
                    success(res) {
                        layer.msg(res.msg)
                        if (res.code === 0)
                            setTimeout(function () {
                                window.location.reload()
                            }, 2500)
                    }
                })
            });
        })

        $('#pathSelect').click(function () {
            layer.open({
                type: 2,
                title: '目录浏览',
                shadeClose: true,
                shade: 0.6,
                area: ['80%', '80%'],
                content: '/file/pathSelect?isFolder=true' // iframe 的 url
            });
        })

        var mb = 1048576
        $('.size').each(function () {
            var ele = $(this);
            ele.html((ele.html() / mb).toFixed(4) + 'MB')
        })

    });

</script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>控制后台</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="/component/pear/css/pear.css"/>
  <link rel="stylesheet" href="/admin/css/other/console1.css"/>
</head>
<body class="pear-container">
<table class="layui-table">
  <colgroup>
    <col width="150">
    <col>
  </colgroup>
  <thead>
  <tr>
    <th>name</th>
    <th>value</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>gitee地址</td>
    <td><a href="https://gitee.com/lingkang_top/ciyuan-waf" target="_blank">https://gitee.com/lingkang_top/ciyuan-waf</a></td>
  </tr>
  <tr>
    <td>nginx绑定配置说明</td>
    <td><a href="/view/nginx/config.html" target="_blank">/view/nginx/config.html</a></td>
  </tr>
  <tr>
    <td>nginx常用模版例示</td>
    <td><a href="/view/nginx/config.html" target="_blank">/view/nginx/template.html</a></td>
  </tr>
  </tbody>
</table>

<script src="/component/layui/layui.js"></script>
<script src="/component/pear/pear.js"></script>
<script>
    layui.use(['layer'], function () {
        var $ = layui.jquery
        var layer = layui.layer
    });
</script>
</body>
</html>

<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>查看日志</title>
  <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding: 20px">
<div class="layui-form layui-row layui-col-space16">
  <div class="layui-col-md3">
    <select lay-filter="select-line">
      <option selected value="200">显示200行</option>
      <option value="500">显示500行</option>
      <option value="1000">显示1000行</option>
    </select>
  </div>
  <div class="layui-col-md9" style="line-height: 38px">
      ${realName}
  </div>
</div>
<div style="width: 100%;overflow-y: auto">
  <pre id="log"></pre>
</div>

<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['jquery'], function () {
        var $ = layui.jquery
        var form = layui.form

        see(200)

        function see(line) {
            layer.load(2, {icon: 16, shade: 0.1, time: 500});
            $.ajax({
                data: {name: '${name}', line: line},
                url: '/nginx/log/see',
                type: 'post',
                success(res) {
                    if (res.code === 0) {
                        let newStr = Array.prototype.slice.call(res.data);
                        $('#log').html(newStr.reverse().join(''))
                        layui.code({
                            elem: '#log'
                        });
                    } else {
                        $('#log').html(res.msg)
                    }
                }
            })
        }

        // select 事件
        form.on('select(select-line)', function (data) {
            see(data.value)
        });
    });

</script>
</body>
</html>
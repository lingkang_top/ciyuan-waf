<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>路径选择</title>
    <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding:16px">
<h3>当前路径：<input id="curr" value="${current!}" disabled style="width: 100%;margin-top: 10px"></h3>
<hr class="layui-border-red">
<div class="layui-form">
    <div class="layui-form-item">
        <div class="layui-input-group">
            <div class="layui-input-split layui-input-prefix">
                进入指定路径
            </div>
            <input value="${current!}" id="folder" type="text" placeholder="进入指定目录路径" style="width: 500px"
                   class="layui-input">
            <div class="layui-input-suffix">
                <button class="layui-btn layui-btn-primary" id="toFile">
                    进入
                    <i class="layui-icon layui-icon-right"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<hr class="layui-border-red">
<div style="line-height: 25px">
    <a href="javascript:;" class="back" style="color: red"><b>返回上一级</b></a><br/>
    <div id="list">
    </div>
</div>
<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['jquery']);
    var $ = layui.jquery
    var isFolder = '${isFolder!"false"}'
    var current = $('#curr').val()
    var back = '0'

    // init
    toPath();

    function toPath() {
        layer.load(2, {icon: 16, shade: 0.1, time: 300});
        $.ajax({
            data: {
                "back": back,
                "isFolder": isFolder,
                "path": current
            },
            url: '/file/pathSelect',
            type: 'post',
            async: true,
            success(res) {
                if (res.code === 0) {
                    current=res.data.current
                    $('#curr').val(current)
                    $('#folder').val(current)
                    var list = ''
                    for (var item of res.data.list) {
                        list += '<a href="javascript:;" class="path" style="color: blue">' + item + '</a><br/>'
                    }
                    $('#list').html(list)
                } else {
                    layer.msg(res.msg)
                }
            }
        })
    }

    $('.back').click(function () {
        back = '1';
        toPath();
        back = '0';
    })

    $('#list').click(function (e) {
        var path = $(e.target).html()
        current = path
        toPath()
    })

    $('#toFile').click(function () {
        var folder = $('#folder').val()
        if (folder === '') {
            layer.msg('目录路径不能为空')
            return
        }
        current = folder
        toPath()
    })
</script>
</body>
</html>
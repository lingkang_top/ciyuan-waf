<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Server配置</title>
    <link rel="stylesheet" href="/component/pear/css/pear.css">
</head>
<body style="padding: 10px">

<button type="button" class="layui-btn" id="add">
    <i class="layui-icon layui-icon-add-1"></i> 添加配置
</button>
<button type="button" class="layui-btn layui-btn-normal" id="download">
    <i class="layui-icon layui-icon-download-circle"></i> 下载所有配置
</button>
<#if nginxExistsUpdate=true>
    <span class="layui-badge">存在修改未重启nginx</span>
</#if>
<div class="layui-card" style="margin-top: 10px;">
    <table class="layui-table">
        <colgroup>
            <col width="300">
            <col width="150">
            <col width="180">
            <col>
        </colgroup>
        <thead>
        <tr>
            <th>名称</th>
            <th>大小</th>
            <th>最后修改时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <#list list as item>
            <tr>
                <td>${item.name}</td>
                <td>${item.size}byte</td>
                <td>${item.lastUpdateTime?string("yyyy-MM-dd HH:mm:ss")}</td>
                <td>
                    <button type="button" name="${item.name}" class="see layui-btn layui-btn-sm layui-btn-normal">
                        <i class="layui-icon layui-icon-eye"></i> 查 看
                    </button>
                    <button type="button" name="${item.name}" class="edit layui-btn layui-btn-sm layui-btn-normal">
                        <i class="layui-icon layui-icon-edit"></i> 编 辑
                    </button>
                    <button name="${item.name}" type="button"
                            class="delete layui-btn layui-btn-sm layui-btn-danger">
                        <i class="layui-icon layui-icon-delete"></i> 删 除
                    </button>
                </td>
            </tr>
        </#list>
        </tbody>
    </table>
</div>

<script type="text/javascript" src="/component/layui/layui.js"></script>
<script type="text/javascript" src="/component/pear/pear.js"></script>
<script>
    layui.use(['jquery'], function () {
        var $ = layui.jquery
        layer.load(2, {icon: 16, shade: 0.1, time: 1000});

        $('#add').click(function () {
            var path = $('#path').val()
            layer.open({
                type: 2,
                title: '添加Server',
                shadeClose: true,
                shade: 0.6,
                area: ['80%', '95%'],
                content: '/nginx/server/addConfig'// iframe 的 url
            });
        })
        $('#download').click(function (){
            window.open('/nginx/server/download')
        })

        $('.delete').click(function () {
            var this_ = this;
            layer.confirm('确认要删除此配置吗？', {
                btn: ['确定', '关闭'] //按钮
            }, function () {
                var name = $(this_).attr('name')
                $.ajax({
                    data: {name: name},
                    type: 'post',
                    url: '/nginx/server/deleteConfig',
                    success(res) {
                        layer.msg(res.msg)
                        if (res.code === 0)
                            setTimeout(function () {
                                window.location.reload()
                            }, 2500)
                    }
                })
            });
        })
        $('.edit').click(function () {
            var name = $(this).attr('name')
            layer.open({
                type: 2,
                title: '编辑Server',
                shadeClose: true,
                shade: 0.6,
                area: ['80%', '95%'],
                content: '/nginx/server/editConfig?name=' + name
            });
        })

        $('.see').click(function () {
            var name = $(this).attr('name')
            $.ajax({
                data: {name: name},
                url: '/nginx/see',
                success(res) {
                    if (res.code === 0) {
                        layer.open({
                            type: 1,
                            shadeClose: true,
                            title: res.msg,
                            shade: 0.6,
                            area: ['60%', '75%'], // 宽高
                            content: '<div style="margin: 20px"><h2 style="margin-bottom: 15px">' + res.msg + '</h2>' +
                                '<pre id="seePre" style="width: 90%;line-height: 22px;background-color: antiquewhite;font-weight:bold">'
                                + res.data + '</pre></div>',
                            success: function(layero, index, that){
                                layui.code({
                                    elem: '#seePre'
                                })
                            }
                        });
                    } else
                        layer.msg(res.msg)
                }
            })

        })
    });

</script>
</body>
</html>
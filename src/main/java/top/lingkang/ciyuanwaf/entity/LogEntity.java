package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author lingkang
 * created by 2023/11/28
 */
@Data
@Entity
@Table(name = "log")
public class LogEntity extends BaseTime {
    @Id
    @Column(length = 50)
    private String id;

    @Column(length = 4)
    private Integer type;

    @Column(length = 2048)
    private String content;

    @Column(length = 32)
    private String ip;
}

package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.Data;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2023/9/9
 */
@Data
@MappedSuperclass
public class BaseTime {
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;

    @PrePersist
    public void prePersist() {
        if (createTime == null)
            createTime = new Date();
        if (updateTime == null)
            updateTime = createTime;
        else
            updateTime = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updateTime = new Date();
    }
}

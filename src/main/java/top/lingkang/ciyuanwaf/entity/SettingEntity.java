package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author lingkang
 * created by 2023/11/23
 */
@Data
@Entity
@Table(name = "setting")
public class SettingEntity extends BaseTime{
    @Id
    @Column(length = 50)
    private String id;

    // /nginx
    @Column(length = 225)
    private String nginxPath;

    // /nginx/conf
    @Column(length = 225)
    private String confPath;

    // /nginx/confList
    @Column(length = 225)
    private String confList;

    // /nginx/logs
    @Column(length = 225)
    private String logPath;

    @Column(length = 10)
    private String isInit;

    @Column(length = 50)
    private String oldName;

    @Column(length = 10)
    private String existsUpdate;
}

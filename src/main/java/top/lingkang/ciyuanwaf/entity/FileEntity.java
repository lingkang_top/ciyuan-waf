package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author lingkang
 * Created by 2023/11/26
 */
@Data
@Entity
@Table(name = "file")
public class FileEntity extends BaseTime {
    @Id
    @Column(length = 50)
    public String id;

    @Column(length = 225)
    public String path;

    @Column(length = 225)
    public String originalName;

    @Column(length = 50)
    public String suffix;

    @Column
    public long size;

    @Column(length = 20)
    public String status;
}

package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author lingkang
 * created by 2023/11/23
 */
@Data
@Entity
@Table(name = "conf_history")
public class ConfHistoryEntity extends BaseTime {
    @Id
    @Column(length = 50)
    private String id;

    @Column(length = 225)
    private String fileName;

    @Column(columnDefinition = "TEXT")
    private String newConf;

    @Column(columnDefinition = "TEXT")
    private String oldConf;

    @Column(length = 225)
    private String exeResult;

    @Column(length = 20)
    private String status;

    @Column(length = 20)
    private String type;

}

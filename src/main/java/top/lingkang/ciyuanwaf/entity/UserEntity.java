package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author lingkang
 * created by 2023/11/28
 */
@Data
@Entity
@Table(name = "user")
public class UserEntity extends BaseTime {
    @Id
    @Column(length = 50)
    private String username;
    @Column(length = 64)
    private String password;
    @Column(length = 20)
    private String nickname;
}

package top.lingkang.ciyuanwaf.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author lingkang
 * Created by 2023/11/26
 */
@Data
@Entity
@Table(name = "file_unzip")
public class UnzipFileEntity extends BaseTime {
    @Id
    @Column(length = 50)
    public String id;

    @Column(length = 225)
    public String path;

    @Column(length = 225)
    public String parentId;

    @Column(length = 20)
    public String status;
}

package top.lingkang.ciyuanwaf.config;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.community.dialect.SQLiteDialect;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.sqlite.JDBC;
import top.lingkang.ciyuanwaf.WebApp;
import top.lingkang.ciyuanwaf.utils.terminal.OsUtils;
import top.lingkang.hibernate6.config.HibernateConfiguration;

import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;

/**
 * @author lingkang
 * created by 2023/10/12
 */
@Configuration
public class DbConfig {
    @Bean
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:sqlite:" + WebApp.dir + "data" + File.separator + "cy.db");
        dataSource.setDriverClassName(JDBC.class.getName());
        dataSource.setPassword("123456");
        dataSource.setUsername("root");
        dataSource.setMinimumIdle(1);
        dataSource.setMaximumPoolSize(10);
        return dataSource;
    }

    @Bean
    public SessionFactory sessionFactory(@Inject DataSource dataSource) {
        Properties properties = new Properties();
        // properties.put(AvailableSettings.ISOLATION, 1);
        properties.put(AvailableSettings.DATASOURCE, dataSource);
        properties.put(AvailableSettings.DIALECT, SQLiteDialect.class);

        /**
         * create：表示启动的时候先drop，再create
         * create-drop: 也表示创建，只不过再系统关闭前执行一下drop
         * update: 这个操作启动的时候会去检查schema是否一致，如果不一致会做scheme更新
         * validate: 启动时验证现有schema与你配置的hibernate是否一致，如果不一致就抛出异常，并不做更新
         */
        properties.put(AvailableSettings.HBM2DDL_AUTO, "update");
        // 打印SQL
        if (OsUtils.isWindow())
            properties.put(AvailableSettings.SHOW_SQL, "true");
        HibernateConfiguration configuration = new HibernateConfiguration();
        // 扫描实体类所在的包
        configuration.addScanPackage("top.lingkang.ciyuanwaf.entity");
        // 或者一个个加
        // configuration.addAnnotatedClass(UserEntity.class);
        configuration.setProperties(properties);
        return configuration.buildSessionFactory();
    }

}

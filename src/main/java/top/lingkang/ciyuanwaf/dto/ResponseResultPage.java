package top.lingkang.ciyuanwaf.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author lingkang
 * created by 2023/5/25
 */
@Data
@Accessors(chain = true)
public class ResponseResultPage extends ResponseResult implements Serializable {
    private int page;
    private int size;
    private long total;
}

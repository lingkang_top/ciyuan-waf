package top.lingkang.ciyuanwaf.dto;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;
import lombok.experimental.Accessors;
import top.lingkang.ciyuanwaf.constants.StatusCode;

import java.io.Serializable;

/**
 * @author lingkang
 * Created in 2022/2/19
 */
@Data
@Accessors(chain = true)
public class ResponseResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private int code = StatusCode.SUCCESS;
    private String msg;
    private T data;

    public ResponseResult() {
        super();
    }

    public ResponseResult(String msg) {
        this.msg = msg;
    }

    public ResponseResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseResult success() {
        this.msg = "success";
        return this;
    }

    public ResponseResult success(String msg) {
        this.msg = msg;
        return this;
    }

    public ResponseResult success(T data) {
        this.data = data;
        return this;
    }

    public ResponseResult success(String msg, T data) {
        this.msg = msg;
        this.data = data;
        return this;
    }

    public ResponseResult fail(String msg) {
        code = StatusCode.FAIL;
        this.msg = msg;
        return this;
    }

    public ResponseResult fail(int code, String msg) {
        this.code = code;
        this.msg = msg;
        return this;
    }

    public ResponseResult setData(T data) {
        this.data = data;
        return this;
    }

    public String toJsonString(){
        return JSONObject.toJSONString(this);
    }
}

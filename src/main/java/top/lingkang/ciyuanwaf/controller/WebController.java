package top.lingkang.ciyuanwaf.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import top.lingkang.ciyuanwaf.constants.LogType;
import top.lingkang.ciyuanwaf.dao.UserDao;
import top.lingkang.ciyuanwaf.dto.ResponseResult;
import top.lingkang.ciyuanwaf.entity.UserEntity;
import top.lingkang.ciyuanwaf.service.LogService;
import top.lingkang.ciyuanwaf.utils.CheckUtils;
import top.lingkang.ciyuanwaf.utils.CommonUtils;
import top.lingkang.hibernate6.transaction.Transaction;

/**
 * @author lingkang
 * created by 2023/10/16
 */
@Controller
public class WebController {
    @Inject
    private UserDao userDao;
    @Inject
    private LogService logService;

    @Mapping("/")
    public void index(Context context) throws Throwable {
        if (context.session("isLogin") != null)
            context.render(new ModelAndView("index.ftl"));
        else {
            context.redirect("/login.html");
        }
    }

    @Mapping(value = "/login", method = MethodType.GET)
    public void login(Context context) {
        context.redirect("/login.html");
    }

    @Mapping(value = "/login", method = MethodType.POST)
    public Object login(String username, String password, Context context) {
        CheckUtils.checkNotEmpty(username, "用户名");
        CheckUtils.checkNotEmpty(password, "密码");
        UserEntity user = userDao.findById(username);
        if (user == null || !CommonUtils.md5(password).equals(user.getPassword())) {
            logService.add(LogType.login, "账号或密码错误");
            return new ResponseResult<>().fail("账号或密码错误");
        }
        logService.add(LogType.login, "登录成功");
        context.sessionSet("isLogin", username);
        return new ResponseResult<>("ok");
    }

    @Mapping("/logout")
    public void logout(Context context) {
        context.sessionRemove("isLogin");
        context.redirect("/login.html");
    }

    @Transaction
    @Mapping(value = "/updatePassword", method = MethodType.POST)
    public Object updatePassword(String password, String newPassword, Context context) {
        CheckUtils.checkNotEmpty(password, "密码");
        CheckUtils.checkRangeLength(newPassword, 6, 20, "新密码");
        Object username = context.session("isLogin");
        if (username == null)
            return new ResponseResult<>().fail("不在登录状态！");
        UserEntity user = userDao.findById(username.toString());
        if (user == null)
            return new ResponseResult<>().fail("用户不存在！");
        if (!CommonUtils.md5(password).equals(user.getPassword())) {
            return new ResponseResult<>().fail("原密码错误！");
        }
        user.setPassword(CommonUtils.md5(newPassword));
        userDao.saveOrUpdate(user);
        logService.add(LogType.update_password, "用户修改密码");
        return new ResponseResult<>("修改密码成功，重新登录时请使用新密码");
    }

}

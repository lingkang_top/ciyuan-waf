package top.lingkang.ciyuanwaf.controller;

import cn.hutool.core.util.StrUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import top.lingkang.ciyuanwaf.constants.LogType;
import top.lingkang.ciyuanwaf.dao.LogDao;
import top.lingkang.ciyuanwaf.dto.ResponseResultPage;
import top.lingkang.ciyuanwaf.utils.PageUtils;
import top.lingkang.ciyuanwaf.utils.SqlParams;
import top.lingkang.ciyuanwaf.vo.LogVO;
import top.lingkang.hibernate6.dao.TupleResult;

import java.util.Date;
import java.util.List;

/**
 * @author lingkang
 * Created by 2023/12/3
 */
@Controller
@Mapping("/nginx/history")
public class NginxHistoryController {

    @Inject
    private LogDao logDao;
    @Inject
    private SessionFactory sessionFactory;

    @Mapping(value = "/index", method = MethodType.GET)
    public Object index() {
        return new ModelAndView("history.ftl").put("type", LogType.getTypeVOList());
    }

    @Mapping(value = "/list", method = MethodType.GET)
    public Object list(String type, String ip, Date start, Date end,
                       Integer page, Integer limit) {
        if (page == null || page > 1024)
            page = 1;
        Integer size = limit;
        if (size == null || size < 10 || size > 100)
            size = 10;

        SqlParams params = new SqlParams();
        if (StrUtil.isNotBlank(type))
            params.put("type =", type);
        if (StrUtil.isNotBlank(ip))
            params.put("ip =", ip);
        if (start != null) {
            params.put("createTime >=", start);
        }
        if (end != null) {
            params.put("createTime <= ", end);
        }

        Session session = sessionFactory.openSession();

        String countSql = "select count(e) from LogEntity e where 1=1 ";
        countSql = PageUtils.setKeys(countSql, params);
        Query<Long> longQuery = session.createQuery(countSql, Long.class);
        PageUtils.setParams(longQuery, params);
        Long total = longQuery.getSingleResult();

        ResponseResultPage resultPage = new ResponseResultPage().setPage(page).setSize(size).setTotal(total);
        if (total == 0)
            return new ResponseResultPage().setPage(page).setSize(size).setTotal(0);

        String sql = "select e.id as id,e.type as type,e.content as content," +
                "e.ip as ip,e.createTime as createTime from LogEntity e where 1=1 ";
        sql = PageUtils.setKeys(sql, params);
        sql+=" order by e.createTime desc";
        Query<LogVO> query = session.createQuery(sql)
                .setTupleTransformer(new TupleResult(LogVO.class))
                .setMaxResults(size)
                .setFirstResult((page - 1) * size);
        PageUtils.setParams(query, params);
        List<LogVO> list = query.list();

        return resultPage.setData(list);
    }
}

package top.lingkang.ciyuanwaf.service;

import top.lingkang.ciyuanwaf.constants.LogType;

/**
 * @author lingkang
 * created by 2023/11/28
 */
public interface LogService {
    void add(LogType type,String content);

    void file(LogType type, String path, long size);
}

package top.lingkang.ciyuanwaf.constants;

import top.lingkang.ciyuanwaf.vo.LogTypeVO;

import java.util.ArrayList;
import java.util.List;

public enum LogType {
    start(0, "应用启动"),
    login(1, "用户登录"),
    update_password(2, "用户修改密码"),
    upload_file(3, "上传文件"),
    download_file(4, "下载文件"),
    unzip_file(5, "解压文件"),
    open_file(6, "打开文件"),
    pack_download_file(7, "打包下载文件"),
    pack_download_list_file(8, "打包下载多个文件"),
    delete_file(9, "删除文件或目录"),

    move_file(10, "移动文件或目录"),

    copy_file(11, "复制文件或目录"),
    rename_file(12, "重命名文件或目录"),


    ;
    public String name;
    public int code;

    LogType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private static final List<LogTypeVO> vos = new ArrayList<>();

    public static List<LogTypeVO> getTypeVOList() {
        if (vos.size() != 0)
            return vos;
        for (LogType type : values())
            vos.add(new LogTypeVO(type.code, type.name));
        return vos;
    }

    @Override
    public String toString() {
        return name;
    }
}

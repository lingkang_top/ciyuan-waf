package top.lingkang.ciyuanwaf.constants;

/**
 * @author lingkang
 * Created in 2022/2/26
 * * 状态码：
 * * 0 成功
 * * 1 失败
 * * 2 登录状态失效
 * * 3 权限不足
 * * 4 账户不存在
 */
public class StatusCode {
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
    public static final int TOKEN_INVALID = 2;
    public static final int NO_PERMISSION = 3;
    public static final int NO_EXISTS_USER = 4;
}

package top.lingkang.ciyuanwaf;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;
import org.noear.solon.web.staticfiles.StaticMimes;
import top.lingkang.ciyuanwaf.utils.terminal.TerminalThread;

import java.io.File;

/**
 * @author lingkang
 * created by 2023/9/8
 */
@Slf4j
@SolonMain
public class WebApp {
    // ~/ciyuan-waf/
    public static String dir = System.getProperty("user.dir");
    public static TerminalThread thread;

    public static void main(String[] args) {
        if (!dir.endsWith("/") && !dir.endsWith("\\"))
            dir += File.separator;
        log.info("base path: {}", dir);
        File data = new File(dir + "data");
        if (!data.exists())
            data.mkdirs();
        Solon.start(WebApp.class, args, app -> {
            StaticMimes.add(".yml", "text/plain");
            thread = new TerminalThread();
        });
    }

}

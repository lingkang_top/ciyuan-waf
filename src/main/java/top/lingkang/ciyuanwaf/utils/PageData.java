package top.lingkang.ciyuanwaf.utils;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * created by 2023/12/6
 */
@Data
public class PageData{
    private int page;
    private int size;
    private long total;
    private List<Object> data=new ArrayList();
}

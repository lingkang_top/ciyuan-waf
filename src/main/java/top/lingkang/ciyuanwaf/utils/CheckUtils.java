package top.lingkang.ciyuanwaf.utils;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author lingkang
 * Created by 2023/1/15
 */
public class CheckUtils {

    public static String CHECK_USERNAME = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static String CHECK_PASSWORD = CHECK_USERNAME + "@-_.";

    public static void checkMaxLength(String str, int maxLength, String name) {
        if (str != null && str.length() > maxLength)
            throw new IllegalArgumentException(name + " 长度不能超过 " + maxLength);
    }

    public static void checkRangeLength(String str, int min, int max, String name) {
        if (str == null || str.length() < min || str.length() > max)
            throw new IllegalArgumentException(name + " 不能为空，并且字符长度在 " + min + " ~ " + max + " 之间");
    }

    public static void checkNotEmpty(String str, String name) {
        if (StrUtil.isEmpty(str))
            throw new IllegalArgumentException(name + " 不能为空");
    }

    public static void checkNotNull(Object obj, String name) {
        if (ObjectUtil.isNull(obj))
            throw new IllegalArgumentException(name + " 不能为空");
    }

    public static void checkIsNumber(String str, String name) {
        if (StrUtil.isNotEmpty(str) && !NumberUtil.isNumber(str))
            throw new IllegalArgumentException(name + " 不是数字");
    }

    public static void checkMinNumber(Integer num, int min, String name) {
        if (num == null || num < min) {
            throw new IllegalArgumentException(name + " 至少为 " + min);
        }
    }

    public static void checkUsername(String username, String name) {
        char[] chars = username.toCharArray();
        for (char c : chars) {
            if (CHECK_USERNAME.indexOf(c) == -1) {
                throw new IllegalArgumentException(name + "只能由大小写字母、数字组成");
            }
        }
    }

    public static void checkPassword(String username, String name) {
        char[] chars = username.toCharArray();
        for (char c : chars) {
            if (CHECK_PASSWORD.indexOf(c) == -1) {
                throw new IllegalArgumentException(name + "只能由大小写字母、数字、@-_.等特殊字母组成");
            }
        }
    }


}

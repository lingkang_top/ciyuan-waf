package top.lingkang.ciyuanwaf.utils;

import cn.hutool.core.lang.id.NanoId;

/**
 * @author lingkang
 * created by 2023/10/12
 */
public class IdUtils {
    private static final char[] num = "0123456789".toCharArray();

    // 输出 13+3位随机数
    public static String id() {
        return System.currentTimeMillis() + NanoId.randomNanoId(null, num, 3);
    }

    /**
     * @param size out 13 + size
     */
    public static String id(int size) {
        return System.currentTimeMillis() + NanoId.randomNanoId(null, num, size);
    }
}

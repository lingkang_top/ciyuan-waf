package top.lingkang.ciyuanwaf.utils.terminal;


/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class OsUtils {
    public static OsType osType = getCurrentOsType();

    public static OsType getCurrentOsType() {
        String os = System.getProperty("os.name");
        if (os != null && os.toLowerCase().contains("window")) {
            osType = OsType.WINDOW;
        } else {
            osType = OsType.LINUX;
        }
        return osType;
    }

    public static boolean isWindow(){
        return osType==OsType.WINDOW;
    }
}

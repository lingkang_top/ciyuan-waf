package top.lingkang.ciyuanwaf.utils.terminal;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public interface TerminalEvent {
    void receive(String str);
}

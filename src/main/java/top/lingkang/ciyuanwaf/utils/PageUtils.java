package top.lingkang.ciyuanwaf.utils;

import lombok.Data;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2023/12/3
 */
public class PageUtils {
    /*public static PageData pageSelect(int page, int size, String sql, SqlParams params, Session session){
        PageData data=new PageData();
        int index = sql.toLowerCase().indexOf("from");
        String count="select count(*) "+sql.substring(index);
        count=setKeys(count,params);
        Query<Long> query = session.createQuery(count, Long.class);
        setParams(query,params);
        Long total = session.createQuery(count, Long.class).getSingleResult();
        data.setTotal(total);
        if (total==0)
            return data;

        sql=setKeys(sql,params);
        query = session.createQuery(count, Long.class);
        setParams(query,params);
        Long total = session.createQuery(count, Long.class).getSingleResult();

        return data;
    }*/

    public static Query setParams(Query query, SqlParams params) {
        int i = 1;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(i, entry.getValue());
            i++;
        }
        return query;
    }

    public static String setKeys(String sql, SqlParams params) {
        int i = 1;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            sql += " and " + entry.getKey() + "?" + i;
            i++;
        }
        return sql;
    }

}

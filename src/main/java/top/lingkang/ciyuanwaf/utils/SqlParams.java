package top.lingkang.ciyuanwaf.utils;

import java.util.HashMap;

/**
 * @author lingkang
 * Created by 2023/12/3
 */
public class SqlParams extends HashMap<String,Object> {
    public SqlParams put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}

package top.lingkang.ciyuanwaf.dao;

import top.lingkang.ciyuanwaf.entity.LogEntity;
import top.lingkang.hibernate6.dao.Dao;
import top.lingkang.hibernate6.dao.HibernateDao;

/**
 * @author lingkang
 * created by 2023/11/23
 */
@Dao
public interface LogDao extends HibernateDao<LogEntity> {
}

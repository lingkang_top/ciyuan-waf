package top.lingkang.ciyuanwaf.dao;

import top.lingkang.ciyuanwaf.entity.SettingEntity;
import top.lingkang.hibernate6.dao.Dao;
import top.lingkang.hibernate6.dao.HibernateDao;
import top.lingkang.hibernate6.dao.Query;

/**
 * @author lingkang
 * created by 2023/11/23
 */
@Dao
public interface SettingDao extends HibernateDao<SettingEntity> {
    @Query("select e from SettingEntity e where id='setting'")
    SettingEntity getSetting();
}

package top.lingkang.ciyuanwaf.dao;

import top.lingkang.ciyuanwaf.entity.ConfHistoryEntity;
import top.lingkang.hibernate6.dao.Dao;
import top.lingkang.hibernate6.dao.HibernateDao;

/**
 * @author lingkang
 * created by 2023/11/24
 */
@Dao
public interface ConfHistoryDao extends HibernateDao<ConfHistoryEntity> {
}

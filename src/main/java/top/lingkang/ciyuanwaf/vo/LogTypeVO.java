package top.lingkang.ciyuanwaf.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lingkang
 * created by 2023/12/4
 */
@Data
public class LogTypeVO implements Serializable {
    public LogTypeVO() {
    }

    public LogTypeVO(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private int code;
    private String name;
}

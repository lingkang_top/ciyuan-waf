package top.lingkang.ciyuanwaf.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lingkang
 * created by 2023/11/27
 */
@Data
public class LogListVO implements Serializable {
    private String name;
    private long size;
    private Date lastUpdateTime;
}

package top.lingkang.ciyuanwaf.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lingkang
 * Created by 2023/12/3
 */
@Data
public class LogVO implements Serializable {
    private String id;

    private Integer type;

    private String content;

    private String ip;

    private Date createTime;
}

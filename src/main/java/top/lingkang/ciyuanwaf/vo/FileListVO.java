package top.lingkang.ciyuanwaf.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lingkang
 * created by 2023/11/30
 */
@Data
public class FileListVO implements Serializable {
    private String name;
    private String path;
    private long size;
    private long lastUpdateTime;
    private boolean isFolder;
}

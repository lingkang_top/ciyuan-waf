import cn.hutool.core.io.FastByteArrayOutputStream;
import cn.hutool.core.io.IoUtil;
import com.pty4j.PtyProcess;
import com.pty4j.PtyProcessBuilder;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class Test01 {
    public static void main(String[] args) throws Exception {
        String[] cmd = {"/bin/sh", "-l"};
        Map<String, String> env = new HashMap<>(System.getenv());
        env.put("TERM", "xterm");
        PtyProcess process = new PtyProcessBuilder().setCommand(cmd).setEnvironment(env).start();

        OutputStream os = process.getOutputStream();
        InputStream is = process.getInputStream();
        // ... work with the streams ...
        // wait until the PTY child process is terminated
        int result = process.waitFor();
        System.out.println(result);
        FastByteArrayOutputStream read = IoUtil.read(is, true);
        System.out.println(read.toString());

    }
}

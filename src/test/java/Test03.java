import cn.hutool.core.thread.ThreadUtil;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class Test03 {
    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("结束");
        }));
        ThreadUtil.sleep(10000);
    }
}

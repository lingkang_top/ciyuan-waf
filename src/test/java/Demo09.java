import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class Demo09 {
    public static void main(String[] args) {
        try {
            // CMD命令，查找所有运行中的example.exe进程
            String cmd = "tasklist | findstr \"nginx.exe\"";

            // 使用Runtime.exec执行CMD命令
            Process process = Runtime.getRuntime().exec(cmd);

            // 读取命令输出
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            process.destroy();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

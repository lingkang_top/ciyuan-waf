import cn.hutool.core.util.ClassUtil;

import java.util.Set;

/**
 * @author lingkang
 * created by 2023/9/11
 */
public class Demo03 {
    public static void main(String[] args) {
        Set<Class<?>> classes = ClassUtil.scanPackage("top.lingkang.ciyuanwaf.entity");
        System.out.println(classes);
    }
}

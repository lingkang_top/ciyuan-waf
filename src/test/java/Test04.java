import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import top.lingkang.ciyuanwaf.utils.terminal.TerminalThread;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
@Slf4j
public class Test04 {
    public static void main(String[] args) {
        TerminalThread terminal = new TerminalThread();
        ThreadUtil.sleep(1000);
        //监听事件
        terminal.addEvent(str -> {
            log.info(str);
        });
        ThreadUtil.sleep(1000);
        terminal.send("tasklist | findstr QQ");
        ThreadUtil.sleep(1000);
        terminal.send("echo 123");
        ThreadUtil.sleep(1000);
        terminal.send("dir");
        ThreadUtil.sleep(10000);
        terminal.send("exit");
    }
}

import cn.hutool.core.thread.ThreadUtil;
import top.lingkang.ciyuanwaf.utils.terminal.TerminalThread;
import top.lingkang.ciyuanwaf.utils.terminal.TerminalUtils;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class Test05 {
    public static void main(String[] args) {
        TerminalThread terminal = new TerminalThread();
        System.out.println(TerminalUtils.nginxIsRun(terminal));;
        ThreadUtil.sleep(1000);
        terminal.send("dir");
        ThreadUtil.sleep(5000);
        terminal.send("exit");
    }
}

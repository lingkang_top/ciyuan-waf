import cn.hutool.core.thread.ThreadUtil;
import com.pty4j.PtyProcess;
import com.pty4j.PtyProcessBuilder;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class Test02 {
    private static LinkedBlockingQueue<String> commandQueue = new LinkedBlockingQueue<>();
    public static void main(String[] args) throws Exception{
        Map<String, String> envs = new HashMap<>(System.getenv());
        envs.put("TERM", "xterm");

        // System.setProperty("PTY_LIB_FOLDER", dataDir.resolve("libpty").toString());
        String[] termCommand = "cmd.exe".split("\\s+");
        PtyProcessBuilder ptyProcessBuilder = new PtyProcessBuilder().setCommand(termCommand).setEnvironment(envs);
        PtyProcess process = ptyProcessBuilder.start();


        BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8));
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), StandardCharsets.UTF_8));

        BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

        commandQueue.put("dir\r");
        outputWriter.write(commandQueue.poll());
        outputWriter.flush();
        // ... work with the streams ...
        // wait until the PTY child process is terminated
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int nRead;
                    char[] data = new char[1 * 1024];

                    while ((nRead = errorReader.read(data, 0, data.length)) != -1) {
                        StringBuilder builder = new StringBuilder(nRead);
                        builder.append(data, 0, nRead);
                        System.out.println(builder.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int nRead;
                    char[] data = new char[1 * 1024];

                    while ((nRead = inputReader.read(data, 0, data.length)) != -1) {
                        StringBuilder builder = new StringBuilder(nRead);
                        builder.append(data, 0, nRead);
                        System.out.println(builder.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        ThreadUtil.sleep(2000);

        commandQueue.put("exit\r");
        outputWriter.write(commandQueue.poll());
        outputWriter.flush();

        int result = process.waitFor();
        System.out.println(result);

    }
}

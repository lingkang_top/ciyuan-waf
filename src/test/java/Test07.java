import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lingkang
 * created by 2023/12/6
 */
public class Test07 {
    public static void main(String[] args) {
        String str = "这是一个示例(包含括号)字符串，还有另一个(括号内的)字符串";
        System.out.println(extractBracketContent(str));
    }
    public static String extractBracketContent(String str) {
        String regex = "\\(([^()]+)\\)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}

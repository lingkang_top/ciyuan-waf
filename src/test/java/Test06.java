import java.util.Properties;

/**
 * @author lingkang
 * Created by 2023/12/1
 */
public class Test06 {
    public static void main(String[] args) {
        Properties properties = System.getProperties();
        properties.remove("java.class.path");
        properties.remove("java.library.path");
        System.out.println(properties);
        System.out.println(properties.toString().length());
    }
}

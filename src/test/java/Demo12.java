import cn.hutool.core.util.StrUtil;

import java.io.File;
import java.util.Arrays;

/**
 * @author lingkang
 * created by 2023/11/29
 */
public class Demo12 {
    public static void main(String[] args) {
        System.out.println(System.getProperty("java.io.tmpdir"));
        String[] split = System.getProperty("user.dir").split("\\\\");
        if (split.length > 1)
            System.out.println(split[split.length - 1]);
        else
            System.out.println("not found");
        File file = new File("d://123");
        String[] list = file.list();
        String string = Arrays.toString(list);
        System.out.println(string);
        file = new File("d://");
        list = file.list();
        string = Arrays.toString(list);
        System.out.println(string);

        String f = "1701244284012_asdasd";
        System.out.println(f.substring(14));
        System.out.println(f.substring(0, 13));
        StrUtil.isBlank("");
    }

}

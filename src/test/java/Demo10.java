import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * @author lingkang
 * Created by 2023/11/25
 */
public class Demo10 {
    public static void main(String[] args) {
        try {
            // 创建ProcessBuilder对象，并设置命令
            ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", "dir D:\\");

            // 启动进程
            Process process = pb.start();

            // 读取进程输出
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            // 等待进程结束
            int exitCode = process.waitFor();
            System.out.println("Exited with code: " + exitCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

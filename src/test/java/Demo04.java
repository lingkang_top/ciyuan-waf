import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.LongAdder;

/**
 * @author lingkang
 * created by 2023/9/15
 */
public class Demo04 {
    public static void main(String[] args) {
        Map map=new HashMap();
        map.put("key","value");
        map.forEach((o, o2) -> {
            System.out.println("key="+o+", value="+o2);
        });
        Random random=new Random();
        System.out.println(random.nextInt(10));
        System.out.println(random.nextInt(10));
        System.out.println(random.nextInt(10));
        System.out.println(Instant.now());
        System.out.println(System.nanoTime());
    }
}
